//
//  TranspireABCTests.swift
//  TranspireABCTests
//
//  Created by Jake Sulkoske on 1/8/18.
//  Copyright © 2018 Sulk. All rights reserved.
//

import XCTest
@testable import TranspireABC

class TranspireABCTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testAppFunction(){
        
    }
    
    func testFormatDate(){
        let testDate = "2018-04-18 14:10:17"
        let formatDate = FormatDate()
        let formattedDate = formatDate.formatPublishDate(date: testDate)
        
        XCTAssertEqual(formattedDate, "Apr 18, 2018 10:10 AM")
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
            let imageUrl = "http://www.abc.net.au/news/image/5238250-4x3-140x105.jpg"
            let testImageView = UIImageView()
            testImageView.getThumbnails(urlString: imageUrl)
        }
    }
    
}
