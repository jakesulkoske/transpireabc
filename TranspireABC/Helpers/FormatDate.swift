//
//  DateFormatter.swift
//  TranspireABC
//
//  Created by Jake Sulkoske on 1/9/18.
//  Copyright © 2018 Sulk. All rights reserved.
//

import UIKit

class FormatDate {
    func formatPublishDate(date: String) -> String {
        let RFC3339DateFormatter = DateFormatter()
        RFC3339DateFormatter.locale = Locale(identifier: "en_US_POSIX")
        RFC3339DateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        RFC3339DateFormatter.timeZone = TimeZone(secondsFromGMT: 0)
        guard let dateObject = RFC3339DateFormatter.date(from: date) else {
            fatalError("ERROR: Date conversion failed due to mismatched format.")
        }
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM d, yyyy hh:mm a"
        let formattedDate = dateFormatter.string(from: dateObject)
        
        return formattedDate
    }
}
