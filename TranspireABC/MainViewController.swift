//
//  MainViewController.swift
//  TranspireABC
//
//  Created by Jake Sulkoske on 1/8/18.
//  Copyright © 2018 Sulk. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    @IBOutlet weak var abcTableView: UITableView!
    
    private let abcUrlString = "https://api.rss2json.com/v1/api.json?rss_url=http://www.abc.net.au/news/feed/51120/rss.xml"
    private var abcArticles = [Article]()
    private var refreshControl: UIRefreshControl!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        getABCData()
        
        //Pull to refresh implementation
        refreshControl = UIRefreshControl()
        refreshControl.attributedTitle = NSAttributedString(string: "Pull to refresh")
        refreshControl.addTarget(self, action: #selector(getABCData), for: .valueChanged)
        abcTableView.refreshControl = refreshControl
        
        //Refresh on entering foreground implementation
        NotificationCenter.default.addObserver(self, selector: #selector(getABCData), name: NSNotification.Name.UIApplicationWillEnterForeground, object: nil )
        
        //Set the B2 Logo in nav bar
        navigationItem.titleView = UIImageView(image: UIImage(named: "B2Image"))
        
    }
    
    @objc func getABCData(){
        //Fetch data from url
        guard let url = URL(string: abcUrlString) else { return }
        URLSession.shared.dataTask(with: url) { data, response, err in
            guard let data = data else { return }
            do{
                //Parse data
                let objects = try JSONDecoder().decode(ABCNewsFeed.self, from: data)
                self.abcArticles = objects.items
                //Async call to reload the data and end refreshing once data is retrieved and parsed
                DispatchQueue.main.async {
                    self.abcTableView.reloadData()
                    self.refreshControl.endRefreshing()
                }
            } catch let err {
                print(err)
            }
        }.resume()
    }
    
}

//Extension for TableView components
extension MainViewController: UITableViewDelegate, UITableViewDataSource {
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return abcArticles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //If table row is 0: generate MainHeaderCell. Else: generate MainTableViewCell
        if indexPath.row == 0 {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell2", for: indexPath) as! MainHeaderCell
            cell.setCell(article: abcArticles[indexPath.row])
            return cell
        } else {
            let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! MainTableViewCell
            cell.setCell(article: abcArticles[indexPath.row])
            return cell
        }
    }
}





