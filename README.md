# README #

An iOS application that displays articles from an ABC news endpoint.
 
- Lists articles from ABC website
- Displays image, title and timestamp
 
## Build Requirements
 
Xcode 8.0 (iOS 11.2) or later
 
## Runtime Requirements
 
iOS 11.2

## Contact

* jakesulkoske@gmail.com