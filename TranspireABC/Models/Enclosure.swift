//
//  Enclosure.swift
//  TranspireABC
//
//  Created by Jake Sulkoske on 1/9/18.
//  Copyright © 2018 Sulk. All rights reserved.
//

import Foundation

struct Enclosure: Decodable {
    let link : String?
    let type : String?
    let thumbnail : String?
}
