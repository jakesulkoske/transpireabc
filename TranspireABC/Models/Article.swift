//
//  Article.swift
//  TranspireABC
//
//  Created by Jake Sulkoske on 1/8/18.
//  Copyright © 2018 Sulk. All rights reserved.
//

import Foundation

struct Article: Decodable {
    let title : String?
    let pubDate : String?
    let enclosure : Enclosure?
}
