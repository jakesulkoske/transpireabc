//
//  MainHeaderCell.swift
//  TranspireABC
//
//  Created by Jake Sulkoske on 1/9/18.
//  Copyright © 2018 Sulk. All rights reserved.
//

import UIKit

class MainHeaderCell: UITableViewCell {
    @IBOutlet weak var articleImageView: UIImageView!
    @IBOutlet weak var articleTitleLabel: UILabel!
    @IBOutlet weak var articlePublishDateLabel: UILabel!
    
    func setCell(article: Article){
        let formattedDate = FormatDate().formatPublishDate(date: article.pubDate!)
        
        articleImageView.getThumbnails(urlString: article.enclosure!.link!)
        articleTitleLabel.text = article.title!
        articlePublishDateLabel.text = formattedDate
    }
}
